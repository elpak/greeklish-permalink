# Greeklish-permalink

This Wordpress plugin converts Greek characters in post, page and term slugs to Latin characters. [Greeklish](https://wordpress.org/plugins/greeklish-permalink/). More info in my [blog7](https://blog7.org).